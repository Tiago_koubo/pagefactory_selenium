package Modules;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx4j.tools.config.DefaultConfigurationBuilder.New;

import org.junit.After;
import org.junit.Rule;
import org.junit.internal.AssumptionViolatedException;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.sun.jna.platform.win32.Sspi.TimeStamp;

    
	public class WatchTest extends TestWatcher {
      
	   // A little set up to give the report a date in the file name
	    public DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        public Date date = new Date();
        String fileDate = dateFormat.format(date);
        public String reportName = "./Test_Report_" + fileDate + ".html";
	    public ExtentReports extent;
	    public Description description;
	    ArrayList<ExtentTest> testList = new ArrayList<>();

	    public WatchTest() {
	        extent = createReport();
	    }	   	   
	    	     

	        // If test passed, watcher will record this with Extent Reports
	        @Override
	        protected void succeeded(Description description) {
	            ExtentTest test = extent.startTest(description.getMethodName());
	            test.log(LogStatus.PASS, "Test Run Successful");
	            flushReports(extent, test);
	            
	        }

	        // Likewise in case of failure
	        @Override
	        protected void failed(Throwable e, Description description) {
	            ExtentTest test = extent.startTest(description.getMethodName());
	            test.log(LogStatus.FAIL, "Test Failure: " + e.toString());
	            flushReports(extent, test);
	        }        
	               

	    /**
	     * These methods do the legwork - this file is based off of an example I found @ www.kieftsoft.nl/?p=81
	     * Eventually we can add some screenshot logic here, but just a clean test report and version info will do
	     */
       
	    private ExtentReports createReport() {
	        // Create the report - Extent just needs a little config
	        ExtentReports extent = new ExtentReports(reportName, false);	       
	        extent.config().reportName("Test Report: "+ fileDate);
	        return extent;
	    }

	    public void flushReports(ExtentReports extent, ExtentTest test) {
	        // This ends the test and then sends (flushes) everything to the html document
	    	 extent.endTest(test);
	         extent.flush();
	       
	    }

	    public List<ExtentTest> getTests() {
	        return testList;
	    }
	}


