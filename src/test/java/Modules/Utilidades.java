package Modules;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Utilidades {
	
	
		
	public static WebDriver Driver(String Browser){		
		 WebDriver driver = null;
		try{		
			
				
				Runtime.getRuntime().exec("taskkill /F /IM firefox.exe");
				//Runtime.getRuntime().exec("taskkill /F /IM chrome.exe");
				Runtime.getRuntime().exec("taskkill /F /IM iexplore.exe");
				Runtime.getRuntime().exec("taskkill /F /IM excel.exe");				
			
			
			switch(Browser){
			case "Firefox" :
				driver = new FirefoxDriver();
				System.out.print("Novo Firefoxdriver instanciado");
				driver.manage().window().maximize();
				System.out.print("Driver setado para abrir Maximizado");
				driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			//	System.out.print("Espera implicita aplicada sobre o driver de " + Constant.ImplicitlyTimeout + " segundos");
				//Acessa URL da Aplicacao				
				driver.get("http://www.google.com.br");
				break;
			}
		}catch (Exception e){
			System.out.print("Não foi possivel abrir o Driver: " + e.getMessage());
		}
	
		return driver;
	}

}
