package Testes;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import Modules.RegrasExecucao;
import Modules.Utilidades;
import Modules.WatchTest;
import PageFactory.PaginaPesquisa;

public class Pesquisar extends RegrasExecucao{
	public static WebDriver driver;
	PaginaPesquisa objpesquisar;
	
	@BeforeClass
	public static void setup(){		
		driver= Utilidades.Driver("Firefox");	
	}

	/**
	 * This test go to http://demo.guru99.com/V4/
	 * Verify login page title as guru99 bank
	 * Login to application
	 * Verify the home page using Dashboard message
	 */
	@Test
	public void Step01_teste_pesquisa_google(){
		objpesquisar.CampoPesquisar.isDisplayed();
	}
	@Test		
	    public void Step02_display_pesquisa_google(){
		
		objpesquisar = new PaginaPesquisa(driver);
		objpesquisar.CampoPesquisar.click();
		objpesquisar.PreencherPesquisa("Oi");
	}
}
